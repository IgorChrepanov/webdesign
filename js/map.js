function initMap() {
  var center = {lat: 50.032886, lng: 9.642857};
  var locations = [
    ['Bautrockner-Verleih<br>\
    Bulmannstraße 36, 90459 Nürnberg<br>\
   <a href="https://g.page/PBTV1?share" target="_blank">Route berechnen</a>',   49.439621, 11.083620],
    ['Bautrockner-Verleih<br>\
    Buderusstraße 27, 59427 Unna<br>\
   <a href="https://g.page/PBTV2?share" target="_blank">Route berechnen</a>', 51.552710, 7.637900],
    ['Bautrockner-Verleih<br>\
    Annastraße 13A, 97072 Würzburg<br>\
    <a href="https://g.page/PBTV3-1?share" target="_blank">Route berechnen</a>', 49.793150, 9.946860],
    ['Bautrockner-Verleih<br>\
    Hanauer Landstraße 314, 60314 Frankfurt<br>\
    <a href="https://g.page/PBTV4?share" target="_blank">Route berechnen</a>', 50.120571, 8.737807],
    ['Bautrockner-Verleih<br>\
    Fürstenrieder Straße 141, 80686 München<br>\
   <a href="https://goo.gl/maps/hziK6hSoQAo7NYhD8" target="_blank">Route berechnen</a>', 48.1275741, 11.503082],
   ['Bautrockner-Verleih<br>\
    Am Mittleren Moos 13, 86167 Augsburg<br>\
   <a href="https://bautrockneraugsburg.de" target="_blank">Route berechnen</a>',   48.393989, 10.935424],
  ];
var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 6.1,
    center: center
  });
var infowindow =  new google.maps.InfoWindow({});
var marker, count;
for (count = 0; count < locations.length; count++) {
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(locations[count][1], locations[count][2]),
      map: map,
      title: locations[count][0]
    });
google.maps.event.addListener(marker, 'click', (function (marker, count) {
      return function () {
        infowindow.setContent(locations[count][0]);
        infowindow.open(map, marker);
      }
    })(marker, count));
  }
}