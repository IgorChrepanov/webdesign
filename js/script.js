var elmnt1 = document.getElementById("content1");
var elmnt2 = document.getElementById("content2");
var elmnt3 = document.getElementById("content3");
var elmnt4 = document.getElementById("content4");
var elmnt5 = document.getElementById("content5");
var elmnt6 = document.getElementById("content6");

var prevScrollpos = window.pageYOffset;
	// HTML document is loaded. DOM is ready.
	$(function() {  

		$.scrollUp({
				scrollName: 'scrollUp',      // Element ID
				scrollDistance: 1,         // Distance from top/bottom before showing element (px)
				scrollFrom: 'top',           // 'top' or 'bottom'
				scrollSpeed: 1,            // Speed back to top (ms)
				easingType: null,        // Scroll to top easing (see http://easings.net/)
				animation: 'fade',           // Fade, slide, none
				animationSpeed: 1,         // Animation speed (ms)		        
				scrollText: '', // Text for element, can contain HTML		        
				scrollImg: true            // Set true to use image		        
		});

		// ScrollUp Placement
		$( window ).on( 'scroll', function() {
			var currentScrollPos = window.pageYOffset;

			if (document.body.scrollTop > 0 || document.documentElement.scrollTop > 30) {
			myHeader.classList.add("sticky");
			myHeader1.classList.add("sticky1");
			space.classList.add("space");
			} else {
			myHeader.classList.remove("sticky");
			myHeader1.classList.remove("sticky1");
			space.classList.remove("space");
			}
			prevScrollpos = currentScrollPos;
			// If the height of the document less the height of the document is the same as the
			// distance the window has scrolled from the top...
			if ( $( document ).height() - $( window ).height() === $( window ).scrollTop() ) {

				// Adjust the scrollUp image so that it's a few pixels above the footer
				$('#scrollUp').css( 'bottom', '30px' );

			} else {      
				// Otherwise, leave set it to its default value.
				$('#scrollUp').css( 'bottom', '30px' );        
			}
		});

	});
function scroll1() {
  document.getElementById("home").scrollIntoView(true); // Top
}
function scroll2() {
  document.getElementById("soforthilfe").scrollIntoView(true); // Top
}
function scroll3() {
  document.getElementById("sortiment").scrollIntoView(true); // Top
}
function scroll4() {
  document.getElementById("sparpakete").scrollIntoView(true); // Top
}
function scroll5() {
	//console.log(document.getElementById("uber_uns"));
  document.getElementById("leistungen").scrollIntoView(true); // Top
}
function scroll6() {
  document.getElementById("anschrift").scrollIntoView(true); // Top
  //2000.scrollIntoView(true); // Top
}

function show(){
	var disp = document.getElementById("myHeader1").style.display;
	if(disp == "none"){
	document.getElementById("myHeader1").style.display = "block";
	}
	if(disp == "block"){
	document.getElementById("myHeader1").style.display = "none";

	}
}

// function hide750(){
// 	var w = window.innerWidth;
// 	if(w > 750)
// 	{
// 		document.getElementById("myHeader1").style.display="none";
// 	}
// }

// var modal = document.getElementById('myHeader1');

// window.onclick = function(event) {
//   if (event.target == modal) {
//     modal.style.display = "none";
//   }
// }

function img1()
{
	$('#imgbutton4').css('height','40px');
	$('#bigtitle1').css('display','none');
	$('#imgbutton5').css('height','40px');
	$('#bigtitle2').css('display','none');

	$('#firstdetail').css('display','block');
	$('#imgbutton1').css('height','125px');
	$('#seconddetail').css('display','block');
	$('#imgbutton2').css('height','125px');

	document.getElementById("imgchange").src="images/sortiment6.png";
	document.getElementById("pr_tt").innerText="80 EUR";
	document.getElementById("pr_dt").innerText="this is smalltext1";
	document.getElementById("imgbutton1").style.backgroundColor="#f2902e";
	document.getElementById("imgbutton2").style.backgroundColor="#ffffff";
	document.getElementById("imgbutton3").style.backgroundColor="#ffffff";
	document.getElementById("imgbutton4").style.backgroundColor="#ffffff";
	document.getElementById("imgbutton5").style.backgroundColor="#ffffff";
}

function img2()
{
	$('#imgbutton4').css('height','40px');
	$('#bigtitle1').css('display','none');
	$('#imgbutton5').css('height','40px');
	$('#bigtitle2').css('display','none');

	$('#firstdetail').css('display','block');
	$('#imgbutton1').css('height','125px');
	$('#seconddetail').css('display','block');
	$('#imgbutton2').css('height','125px');
	document.getElementById("imgchange").src="images/sortiment9.jpg";
	document.getElementById("pr_tt").innerText="90 EUR";
	document.getElementById("pr_dt").innerText="this is smalltext11";
	document.getElementById("imgbutton1").style.backgroundColor="#ffffff";
	document.getElementById("imgbutton2").style.backgroundColor="#f2902e";
	document.getElementById("imgbutton3").style.backgroundColor="#ffffff";
	document.getElementById("imgbutton4").style.backgroundColor="#ffffff";
	document.getElementById("imgbutton5").style.backgroundColor="#ffffff";
}

function img3()
{
	document.getElementById("imgchange").src="images/DSC_0728.jpg";
	document.getElementById("pr_tt").innerText="180 EUR";
	document.getElementById("pr_dt").innerText="this is smalltext111";
	document.getElementById("imgbutton1").style.backgroundColor="#ffffff";
	document.getElementById("imgbutton2").style.backgroundColor="#ffffff";
	document.getElementById("imgbutton3").style.backgroundColor="#f2902e";
	document.getElementById("imgbutton4").style.backgroundColor="#ffffff";
	document.getElementById("imgbutton5").style.backgroundColor="#ffffff";
	
}

function img4()
{
	// document.getElementById("imgchange").src="images/sortiment4.jpg";
	$('#imgchange').attr('src',"images/sortiment4.jpg");
	$('#firstdetail').css('display','none');
	$('#imgbutton1').css('height','40px');
	$('#imgbutton4').css('height','125px');
	$('#bigtitle1').css('display','block');
	$('#imgbutton5').css('height','40px');
	$('#bigtitle2').css('display','none');

	$('#imgbutton5').css('height','40px');
	$('#bigtitle2').css('display','none');
	$('#seconddetail').css('display','block');
	$('#imgbutton2').css('height','125px');
	document.getElementById("pr_tt").innerText="45 EUR";
	document.getElementById("pr_dt").innerText="this is smalltext44";
	document.getElementById("imgbutton1").style.backgroundColor="#ffffff";
	document.getElementById("imgbutton2").style.backgroundColor="#ffffff";
	document.getElementById("imgbutton3").style.backgroundColor="#ffffff";
	document.getElementById("imgbutton4").style.backgroundColor="#f2902e";
	document.getElementById("imgbutton5").style.backgroundColor="#ffffff";
}
function img5()
{
	$('#imgchange').attr('src',"images/sortiment8.jpg");
	$('#seconddetail').css('display','none');
	$('#firstdetail').css('display','none');
	$('#imgbutton2').css('height','40px');
	$('#imgbutton1').css('height','40px');
	$('#imgbutton5').css('height','125px');
	$('#bigtitle2').css('display','block');

	$('#imgbutton4').css('height','125px');
	$('#bigtitle1').css('display','block');
	// $('#firstdetail').css('display','block');
	// $('#imgbutton1').css('height','125px');
	document.getElementById("pr_tt").innerText="20 EUR";
	document.getElementById("pr_dt").innerText="this is smalltext55";
	document.getElementById("imgbutton1").style.backgroundColor="#ffffff";
	document.getElementById("imgbutton2").style.backgroundColor="#ffffff";
	document.getElementById("imgbutton3").style.backgroundColor="#ffffff";
	document.getElementById("imgbutton4").style.backgroundColor="#ffffff";
	document.getElementById("imgbutton5").style.backgroundColor="#f2902e";
}

