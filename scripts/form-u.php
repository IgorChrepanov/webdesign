<?php

      $subject = "Neue Kundenanfrage";
      
      if(!empty($_POST['name'])){
        $name = "Name:\t".$_POST['name']."\r\n\n";
      }else{
        $name = "";
      }

      if(!empty($_POST['phone'])){
        $tel = "Telefon:\t".$_POST['phone']."\r\n\n";
      }else{
        $tel = "";
      }

      $useragent=$_SERVER['HTTP_USER_AGENT'];

      function isMobile() {
          return preg_match("/(android|webos|avantgo|iphone|ipad|ipod|blackberry|iemobile|bolt|boost|cricket|docomo|fone|hiptop|mini|opera mini|kitkat|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
      }

      if(isMobile()){
          $device = "Mobile";
      }
      else {
          $device = "Desktop";
      }

      date_default_timezone_set("Europe/Berlin");
      $datecheck = date("Y-m-d");
      $weekday = date("l", strtotime($datecheck));

      switch ($weekday) {
        case 'Monday':
          $weekday = "Montag";
          break;
        case 'Tuesday':
          $weekday = "Dienstag";
          break;
        case 'Wednesday':
          $weekday = "Mittwoch";
          break;
        case 'Thursday':
          $weekday = "Donnerstag ";
          break;
        case 'Friday':
          $weekday = "Freitag";
          break;
        case 'Saturday':
          $weekday = "Samstag";
          break;
        case 'Sunday':
          $weekday = "Sonntag";
          break;

        default:
          $weekday = "";
          break;
      }

      $date = date("d/m/Y");
      $time = date("H:i");

      $field = $_POST['email'];
      $field = filter_var($field, FILTER_SANITIZE_EMAIL);
      $to_email = "kontakt@bautrockner-nuernberg.de";
      $email = "E-Mail:\t".$field."\n\n";
      // $message = file_get_contents($templatefile);
      $message = $_POST['message'];
      $message = str_replace("\n.", "\n..", $message);
      $message = "\r\n\n\n".$name.$email.$tel."Nachricht:\t".$message."\n\n\nAnfrage gesendet von ".$device." | ".$weekday.", ".$date." um ".$time." Uhr | www.bautrockner-nuemberg.de";
      $subject = mb_convert_encoding($subject, "UTF-8");
      $message = mb_convert_encoding($message, "UTF-8");
      $headers = 'From: '.$field. "\r\n";
      
      mail($to_email, $subject, $message, $headers);
      echo "ok";   
   
?>
